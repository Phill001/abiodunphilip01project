package com.phill.testcases;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.phill.utility.BrowserFactory;

public class LoginTest {
	
	WebDriver driver;
	
	@Test
	public void login() {
		driver = BrowserFactory.startApplication(driver, "Chrome", "https://jupiter.cloud.planittesting.com/#/");
		System.out.println("Title is: " + driver.getTitle());
		BrowserFactory.quitBrowser(driver);
	}

}
